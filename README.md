#Canopy Quote Front End Setup Guide#

###This guide assumes that you are using a machine with Node.JS already installed.###
* #####If you do not have Node installed, please refer to nodejs.org for installation instructions, or install it using Homebrew.
* #####I'm also using NVM to manage the active Node version. Info on NVM can be found here: https://github.com/creationix/nvm
* #####This project was built using Node.js v5.2.0




## How do I get set up? ##

* Install Yeoman, Bower, and Grunt with NPM using the following command:
```
#!bash

$ npm install -g yo bower grunt

```

* Clone the project onto your machine, and, in the bash terminal, cd into the root of the project.

* In the project root in the terminal, execute the following command:
```
#!bash

$ npm install && bower install

```
* Install SASS using the following command:
```
#!bash

$ gem install sass
```

* To run the application, in the project root of the terminal, execute:
```
#!bash

$ grunt serve

```

#####If grunt serve fails because of a missing NPM package, in the root of the project run:
```
#!bash

$  npm install *missing package* --save

```
#### This will install the package and save it in the package.json for subsequent users.

## Where does stuff live? ##
* Page specific JS is located in "client/app"
* Common components(i.e. navigation bar) is located in "client/components"
* The mock quote API is located at "server/api/quotes"

####I'm around on Hangouts if you need to ping me with questions####
