'use strict';

angular.module('canopyApp')
  .controller('GenAppCtrl', function ($state, $stateParams, $http) {
    var vm = this;
    console.log($stateParams.userInfo);

    if($stateParams.userInfo === null){
      $state.go('main');
    }

    vm.userInfo = $stateParams.userInfo;

    vm.policyInfo = {
      carrier: $stateParams.details.carrier,
      amount: $stateParams.userInfo.amount,
      term: $stateParams.details.term,
      premium: $stateParams.premium
    };

    vm.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District of Columbia', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
    vm.homeState = true;

    vm.today = new Date();
    vm.maxDate = new Date(
      vm.today.getFullYear() - vm.userInfo.age,
      vm.today.getMonth(),
      vm.today.getDate());
    vm.minDate = new Date(
      vm.today.getFullYear() - vm.userInfo.age - 1,
      vm.today.getMonth(),
      vm.today.getDate());

    vm.getArray=function(n){
      return new Array(n);
    };

    vm.application = {
      background:{
        bankruptcy:{
          status: false
        },
        declinedCoverage:{
          status: false
        },
        dui: {
          status: false
        },
        felony:{
          status: false
        },
        hazardousActivity:{
          status: false
        },
        insurance:{
          status: false
        },
        insuranceApplication:{
          status: false
        },
        internationalTravel:{
          status: false
        },
        licenseSuspended:{
          status: false
        },
        movingViolations:{
          status: false
        },
        military: false,
        pilot: false
      },
      beneficiaries:{
        primary:{
          firstChoice:{},
          secondChoice:{}
        },
        secondary: {
          firstChoice: {
            estate: true
          },
          secondChoice: {}
        }
      },
      details:{
        citizen: true,
        driversLicense:{
          state: vm.userInfo.state
        },
        employmentDetails: {},
        gender: vm.userInfo.gender,
        married: false
      },
      health:{
        prescriptionDrugs: false,
        bpMedication: false,
        cholesterolMedication: false,
        familyHistory: {
          disease: false,
          earlyDeath: false
        },
        height:{}
      },
      policy: vm.policyInfo
    };

    vm.submit = function(){
      console.log(vm.application);
      var req = {
        method: 'POST',
        url: 'http://127.0.0.1:3000/relay_quote_requests_forms', //'https://stormy-basin-84682.herokuapp.com/relay_quote_requests_forms',
        headers: {'Content-Type': 'application/json'},
        data: vm.application
      };

      $http(req).then(function() {
          // Do nothing for now
        }, function(){});
    };
  });
