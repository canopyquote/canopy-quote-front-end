'use strict';

angular.module('canopyApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngAnimate',
  'ngMessages',
  'ui.router',
  'ngMaterial',
  'ngMask',
  'currencyMask'
])
  .config(function($mdThemingProvider){
    $mdThemingProvider.definePalette('green', {
      '50': '#efffd3',
      '100': '#d2ff87',
      '200': '#bdff4f',
      '300': '#a3ff07',
      '400': '#91e800',
      '500': '#7ec900',
      '600': '#6baa00',
      '700': '#588c00',
      '800': '#446d00',
      '900': '#314f00',
      'A100': '#efffd3',
      'A200': '#d2ff87',
      'A400': '#91e800',
      'A700': '#588c00',
      'contrastDefaultColor': 'light',
      'contrastDarkColors': '50 100 200 300 400 500 600 A100 A200 A400'
    });

    $mdThemingProvider.definePalette('blue', {
      '50': '#6de5db',
      '100': '#2cd9cb',
      '200': '#1fada2',
      '300': '#147169',
      '400': '#105751',
      '500': '#0b3d39',
      '600': '#062321',
      '700': '#020909',
      '800': '#000000',
      '900': '#000000',
      'A100': '#6de5db',
      'A200': '#2cd9cb',
      'A400': '#105751',
      'A700': '#020909',
      'contrastDefaultColor': 'light',
      'contrastDarkColors': '50 100 200 A100 A200'
    });

    $mdThemingProvider.theme('canopy')
      .primaryPalette('green')
      .accentPalette('blue');
  })
  .config(function ($mdThemingProvider) {
    $mdThemingProvider.setDefaultTheme('canopy');
  })
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  })
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl',
        controllerAs: 'ctrl'
      })
      .state('quotes', {
        url: '/quotes',
        templateUrl: 'app/quotes/quotes.html',
        controller: 'QuotesCtrl',
        controllerAs: 'ctrl',
        params: {
          form: {}
        }
      })
      .state('application', {
        url: '/application',
        templateUrl: 'app/gen-app/gen-app.html',
        controller: 'GenAppCtrl',
        controllerAs: 'ctrl',
        params: {
          premium: null,
          userInfo: null,
          details: null
        }
      });
  })
  .config(function ($provide) {
    $provide.decorator('$uiViewScroll', function ($delegate) {
      return function (uiViewElement) {
        window.scrollTo(0, 0);
      };
    });
  }
);
