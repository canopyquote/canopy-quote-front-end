'use strict';

angular.module('canopyApp')
.controller('QuotesCtrl', function ($state, $stateParams, canopyService) {
    var vm = this;
    vm.userInfo = $stateParams.form;
    console.log(vm.userInfo);

    function init(){
      if(Object.keys(vm.userInfo ).length === 0){
        console.log('kickout');
      }else{
        delete vm.userInfo['tobacco'];
        canopyService.getQuotes(vm.userInfo)
          .then(function(result){
            vm.quotes = result;
          });
      }
    }
    init();
  });
