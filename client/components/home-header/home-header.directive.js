(function() {
  'use strict';

  angular
    .module('canopyApp')
    .directive('homeHeader', homeHeader);

  /** @ngInject */
  function homeHeader($state) {
    return {
      restrict: 'E',
      templateUrl: 'components/home-header/home-header.html',
      scope: {
      },
      controller: homeHeaderController,
      controllerAs: 'ctrl',
      bindToController: true
    };

    /** @ngInject */
    function homeHeaderController($rootScope) {
      var ctrl = this;
      ctrl.scroll = function(){
        var el = document.getElementById('main'),
          start = new Date().getTime(),
          timer = setInterval(function() {
            var step = Math.min(1, (new Date().getTime() - start) / 500);
            document.body['scrollTop'] = (step * (el.offsetTop - 10));
            if (step == 1){
              clearInterval(timer);
            }
          }, 5);
      }
    }
  }
})();
