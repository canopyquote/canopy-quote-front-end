'use strict';

describe('Service: canopyService', function () {

  // load the service's module
  beforeEach(module('canopyApp'));

  // instantiate service
  var canopyService;
  beforeEach(inject(function (_canopyService_) {
    canopyService = _canopyService_;
  }));

  it('should do something', function () {
    expect(!!canopyService).toBe(true);
  });

});
