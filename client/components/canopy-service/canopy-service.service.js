'use strict';

angular.module('canopyApp')
  .service('canopyService', function ($http, $state, $q) {
    var model = this;
    Object.prototype.toQueryString = function () {
      var self = this,
        queryString = [];

      _.forEach(self, function(value, key) {
        var result = key + '=' + value;
        queryString.push(result);
      });

      return queryString.join('&');
    };


    model.getQuotes = function(request) {
      var string = request.toQueryString();
      var path = 'api/quotes?' + string,
        req = {
          method: 'GET',
          url: path
        },
        def = $q.defer();
      $http(req).success(function (result) {
        def.resolve(result);
      }).error(function (data) {
        def.reject(data);
      });
      return def.promise;
    };
  });
