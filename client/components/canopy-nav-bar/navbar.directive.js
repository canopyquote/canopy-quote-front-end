(function() {
  'use strict';

  angular
    .module('canopyApp')
    .directive('canopyNavBar', canopyNavBar);

  /** @ngInject */
  function canopyNavBar($state) {
    var directive = {
      restrict: 'E',
      templateUrl: 'components/canopy-nav-bar/navbar.html',
      scope: {
          options: '='
      },
      controller: CanopyNavBarController,
      controllerAs: 'ctrl',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function CanopyNavBarController($rootScope) {
      var ctrl = this;

      ctrl.status = {
        isopen: false
      };

      ctrl.toggled = function(open) {
        $log.log('Dropdown is now: ', open);
      };

      ctrl.toggleDropdown = function($event) {
        console.log('toggle');
        $event.preventDefault();
        $event.stopPropagation();
        ctrl.status.isopen = !ctrl.status.isopen;
      };

      $rootScope.$on('$stateChangeSuccess',
        function(event, toState, toParams, fromState, fromParams){
          ctrl.state = toState.name;
        });
    }
  }

})();
